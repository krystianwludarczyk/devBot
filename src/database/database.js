const sqlite3 = require("sqlite3").verbose();
const api = require("../api/api");

module.exports = {
  pushCurrentPlayersCount: async function (playersCount, date) {
    try {
      const database = new sqlite3.Database("database.sqlite");
      const insertQuery =
        "INSERT INTO online_players (count, date) VALUES (?, ?)";
      database.run(insertQuery, [playersCount, date]);
      database.close();
    } catch (error) {
      console.error(error);
    }
  },
  getLastPlayersCount: async function () {
    return new Promise((resolve, reject) => {
      const database = new sqlite3.Database("database.sqlite");
      const selectQuery =
        "SELECT count, date FROM online_players ORDER BY date DESC LIMIT 1";

      database.get(selectQuery, (err, row) => {
        database.close();

        if (err) {
          console.error(err);
          return reject(err);
        }

        const lastOnlinePlayersNoted = row ? row.count : -1;
        const date = new Date(row.date).toISOString();
        console.log(date);
        console.log(lastOnlinePlayersNoted);
        resolve(lastOnlinePlayersNoted);
      });
    });
  },

  getTopPlayersCount: async function (hoursAgo) {
    const database = new sqlite3.Database("database.sqlite");

    try {
      const cutoffDate = new Date();
      cutoffDate.setHours(cutoffDate.getHours() - hoursAgo);

      const selectQuery =
        "SELECT count, date FROM online_players WHERE date >= ? ORDER BY count DESC LIMIT 1";

      const topRecord = await new Promise((resolve, reject) => {
        database.get(selectQuery, [cutoffDate], (err, row) => {
          if (err) {
            reject(err);
          } else {
            resolve(row);
          }
        });
      });

      database.close();
      return topRecord;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  pushUsersDailyActivity: async function () {
    const activity = await api.getUsersData();
    const newDate = new Date();

    for (const player of activity) {
      const { name, admin } = player;

      if (admin.type === 2) {
        await pushUserActivity(name, newDate, admin.duty);
      }
    }
  },

  getUserActivity: async function (nickname, daysAgo) {
    try {
      const database = new sqlite3.Database("database.sqlite");
      const cutoffDate = new Date();
      cutoffDate.setDate(cutoffDate.getDate() - daysAgo);
      const selectQuery =
        "SELECT SUM(minutes) AS totalMinutes FROM support_activity WHERE nickname = ? AND date >= ?";

      const totalActivityTime = await new Promise((resolve, reject) => {
        database.get(selectQuery, [nickname, cutoffDate], (err, row) => {
          if (err) {
            reject(err);
          } else {
            const totalTime = row.totalMinutes || 0;
            resolve(totalTime);
          }
        });
      });

      database.close();
      return totalActivityTime;
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};

async function pushUserActivity(nickname, date, minutes) {
  try {
    const database = new sqlite3.Database("database.sqlite");
    const insertQuery =
      "INSERT INTO support_activity (nickname, date, minutes) VALUES (?, ?, ?)";
    database.run(insertQuery, [nickname, date, minutes]);
    database.close();
  } catch (error) {
    console.error(error);
  }
}
