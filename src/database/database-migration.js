const sqlite3 = require("sqlite3").verbose();

const database = new sqlite3.Database("database.sqlite", (error) => {
  if (error) {
    console.error("Connected to database failed.", error.message);
  } else {
    console.log("Connected to database successfully.");
  }
});

const createTableQuery_UserActivity = `CREATE TABLE support_activity (
  id INTEGER PRIMARY KEY,
  nickname TEXT NOT NULL,
  date DATE NOT NULL,
  minutes INTEGER NOT NULL
)`;

database.run(createTableQuery_UserActivity, (error) => {
  if (error) {
    console.error(
      "Error while creating the 'support_activity' table.",
      error.message
    );
  } else {
    console.log("Created 'support_activity'.");
  }
});

const createTableQuery_OnlinePlayers = `CREATE TABLE online_players (
  id INTEGER PRIMARY KEY,
  count INTEGER NOT NULL,
  date DATE NOT NULL
)`;

database.run(createTableQuery_OnlinePlayers, (error) => {
  if (error) {
    console.error(
      "Error while creating the 'online_players' table.",
      error.message
    );
  } else {
    console.log("Created 'online_players'.");
  }
});

database.close();
