const { EmbedBuilder } = require("discord.js");

module.exports = {
  build: async function (type, text, messageText) {
    let authorData;
    let color;
    if (type == "error") {
      color = "Red";
      authorData = {
        name: "Błąd",
        iconURL: "https://i.imgur.com/naoyCEy.png",
      };
    } else if (type == "info") {
      color = "Blue";
      authorData = {
        name: "Informacja",
        iconURL: "https://i.imgur.com/bRFQY2c.png",
      };
    }

    //Build Embed
    const embed = new EmbedBuilder()
      .setAuthor(authorData)
      .setDescription(text)
      .setColor(color)
      .setTimestamp()
      .setFooter({
        text: `!help`,
      });

    //Build Message
    const message = {
      content: messageText,
      embeds: [embed],
    };

    return message;
  },
};
