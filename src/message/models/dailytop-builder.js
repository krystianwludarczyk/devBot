const api = require("../../api/api");
const { EmbedBuilder } = require("discord.js");
const messageBuilder = require("../message-builder");
const database = require("../../database/database");

module.exports = {
  build: async function (messageText) {
    try {
      //Variables
      const activity = await api.getUsersData();
      let messageContent = "";

      const topCount = await database.getTopPlayersCount(12);
      const date = new Date(topCount.date);
      messageContent += `:golf: **PEAK GRACZY:**\n${
        topCount.count
      } - ${date.toLocaleTimeString(undefined, {
        hour: "numeric",
        minute: "numeric",
        hour12: false,
      })}\n\n`;

      //Top Of The Day
      messageContent += `:trophy: **TOP DNIA:**\n`;

      let bestActivityTime = 0;
      activity.forEach((player) => {
        if (player.admin.type == 2 && player.admin.duty > bestActivityTime) {
          bestActivityTime = player.admin.duty;
        }
      });

      let supporterData = [];
      activity.forEach((player) => {
        if (
          bestActivityTime > 0 &&
          player.admin.type == 2 &&
          player.admin.duty >= bestActivityTime
        ) {
          supporterData.push(player);
        }
      });

      if (supporterData.length > 0) {
        supporterData.forEach((player) => {
          messageContent += `• (S${player.admin.level}) **${player.name}** - ${player.admin.duty} minut\n`;
        });
      } else {
        messageContent += `• brak\n`;
      }
      messageContent += `\n`;

      //Inactive Today
      messageContent += `:warning: **DZISIAJ NIEAKTYWNI:**\n`;
      activity.forEach((player) => {
        let percentageDuty;
        if (player.admin.duty === 0 && player.gameTimeToday === 0) {
          percentageDuty = 0;
        } else {
          percentageDuty = Math.min(
            100,
            Math.max(
              0,
              Math.round((player.admin.duty / player.gameTimeToday) * 100)
            )
          );
        }

        if (
          player.admin.type == 2 &&
          (player.gameTimeToday != 0 ||
            player.admin.duty != 0 ||
            player.inGame) &&
          (player.admin.duty < 15 || percentageDuty < 15)
        ) {
          messageContent += `• (S${player.admin.level}) **${player.name}**: ${player.admin.duty}/${player.gameTimeToday} (${percentageDuty}%) \n`;
        }
      });
      messageContent += `\n`;

      //Online Today
      messageContent += `:green_circle: **DZISIAJ ONLINE:** \n`;
      activity.forEach((player) => {
        if (
          player.admin.type == 2 &&
          (player.gameTimeToday != 0 || player.admin.duty != 0 || player.inGame)
        ) {
          let percentageDuty;
          if (player.admin.duty === 0 && player.gameTimeToday === 0) {
            percentageDuty = 0;
          } else {
            percentageDuty = Math.min(
              100,
              Math.max(
                0,
                Math.round((player.admin.duty / player.gameTimeToday) * 100)
              )
            );
          }
          messageContent += `• (S${player.admin.level}) **${player.name}**: ${player.admin.duty}/${player.gameTimeToday} (${percentageDuty}%) \n`;
        }
      });
      messageContent += `\n`;

      //Offline Today
      messageContent += `:red_circle: **DZISIAJ OFFLINE:** \n`;
      activity.forEach((player) => {
        if (
          player.admin.type == 2 &&
          player.gameTimeToday == 0 &&
          player.admin.duty == 0 &&
          player.inGame == false
        ) {
          messageContent += `• (S${player.admin.level}) **${player.name}**\n`;
        }
      });
      messageContent += `\n`;

      //Build Embed
      const embed = new EmbedBuilder()
        .setThumbnail("https://i.imgur.com/TcycwTW.png")
        .setTitle("**WYKAZ AKTYWNOŚCI SUPPORTERÓW**")
        .setDescription(messageContent)
        .setColor("Blue")
        .setTimestamp()
        .setFooter({
          text: `!help`,
        });

      //Build Raport
      const raport = {
        content: messageText,
        embeds: [embed],
      };
      return raport;
    } catch (error) {
      console.error(error);
      return await messageBuilder.build(
        "error",
        "Wystąpił nieoczekiwany błąd.",
        ""
      );
    }
  },
};
