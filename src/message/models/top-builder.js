const api = require("../../api/api");
const { EmbedBuilder } = require("discord.js");
const messageBuilder = require("../message-builder");
const database = require("../../database/database");

module.exports = {
  build: async function (days) {
    try {
      if (!days) {
        days = 7;
      } else if (days < 2 || days > 60) {
        return await messageBuilder.build(
          "error",
          "Wykaz czasowy musi obejmować od 2 do 60 dni.",
          ""
        );
      }

      const activity = await api.getUsersData();

      let bestTime = 1;
      let playersData = [];

      // Pobieramy dane o aktywności dla każdego gracza i jednocześnie szukamy najlepszego czasu
      for (const player of activity) {
        if (player.admin.type == 2) {
          const timeTotal = await database.getUserActivity(player.name, days);
          if (timeTotal > bestTime) {
            bestTime = timeTotal;
            playersData = [player];
          } else if (timeTotal == bestTime) {
            playersData.push(player);
          }
        }
      }

      let messageContent = `**:trophy: TOP:**\n`;
      if (playersData.length > 0) {
        playersData.forEach((player) => {
          messageContent += `• (S${player.admin.level}) **${player.name}** - ${bestTime} minut\n`;
        });
      } else {
        messageContent += `• brak\n`;
      }
      messageContent += `\n`;

      // Wyświetlamy dane wszystkich graczy typu admin.type == 2
      messageContent += `**:ledger: WSZYSCY:**\n`;
      for (const player of activity) {
        if (player.admin.type == 2) {
          const timeTotal = await database.getUserActivity(player.name, days);
          messageContent += `• (S${player.admin.level}) **${player.name}** - ${timeTotal} minut\n`;
        }
      }

      //Build Embed
      const embed = new EmbedBuilder()
        .setThumbnail("https://i.imgur.com/TcycwTW.png")
        .setTitle(`**OSTATNIE ${days} DNI**`)
        .setDescription(messageContent)
        .setColor("Blue")
        .setTimestamp()
        .setFooter({
          text: `!help`,
        });

      //Build Raport
      const raport = {
        content: "",
        embeds: [embed],
      };
      return raport;
    } catch (error) {
      console.error(error);
      return await messageBuilder.build(
        "error",
        "Wystąpił nieoczekiwany błąd.",
        ""
      );
    }
  },
};
