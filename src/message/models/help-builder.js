const { EmbedBuilder } = require("discord.js");
const messageBuilder = require("../message-builder");

module.exports = {
  build: async function (messageText) {
    try {
      let messageContent = "";
      messageContent += `• **!help**\nlista dostępnych komend\n\n`;
      messageContent += `• **!online**\nstatus serwera\n\n`;
      messageContent += `• **!dailytop**\nwykaz aktywności użytkowników z obecnego dnia\n\n`;
      messageContent += `• **!top** [dni]\nwykaz aktywności użytkowników z podanych dni\n\n`;
      messageContent += `• **!user** [nick]\ninformacje o użytkowniku\n\n`;

      messageContent += `:arrow_forward: Autor: <@650426675200393258>\n`;

      //Build Embed
      const embed = new EmbedBuilder()
        .setThumbnail("https://i.imgur.com/vN6EjuM.png")
        .setTitle("**LISTA DOSTĘPNYCH KOMEND**")
        .setDescription(messageContent)
        .setColor("Blue")
        .setTimestamp()
        .setFooter({
          text: `!help`,
        });

      //Build Raport
      const reply = {
        embeds: [embed],
      };
      return reply;
    } catch (error) {
      console.error(error);
      return await messageBuilder.build(
        "error",
        "Wystąpił nieoczekiwany błąd.",
        ""
      );
    }
  },
};
