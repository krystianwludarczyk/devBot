const api = require("../../api/api");
const { EmbedBuilder } = require("discord.js");
const messageBuilder = require("../message-builder");
const database = require("../../database/database");

module.exports = {
  build: async function () {
    try {
      let messageContent = "";
      const activity = await api.getUsersData();

      const playersCount = await database.getLastPlayersCount();
      messageContent += `:bar_chart: **Ilość graczy:**\n${playersCount} / 1000\n`;
      messageContent += `\n`;

      //Administrator
      let administratorData = [];
      activity.forEach((player) => {
        if (player.admin.type === 1) {
          administratorData.push(player);
        }
      });
      messageContent += buildFor(
        "red",
        "A",
        "Administrator",
        administratorData
      );

      //Community Manager
      let communityManagerData = [];
      activity.forEach((player) => {
        if (player.admin.type === 3 && player.admin.level <= 6) {
          communityManagerData.push(player);
        }
      });
      messageContent += buildFor(
        "green",
        "CM",
        "Community Manager",
        communityManagerData
      );

      //Gamemaster
      let gamemasterData = [];
      activity.forEach((player) => {
        if (player.admin.type === 3 && player.admin.level >= 7) {
          player.admin.level -= 6;
          gamemasterData.push(player);
        }
      });
      messageContent += buildFor("purple", "GM", "Gamemaster", gamemasterData);

      //Developer
      let developerData = [];
      activity.forEach((player) => {
        if (player.admin.type === 4) {
          developerData.push(player);
        }
      });
      messageContent += buildFor("brown", "D", "Developer", developerData);

      //Supporter
      let supporterData = [];
      activity.forEach((player) => {
        if (player.admin.type === 2) {
          supporterData.push(player);
        }
      });
      messageContent += buildFor("blue", "S", "Supporter", supporterData);

      //Build Embed
      const embed = new EmbedBuilder()
        .setThumbnail("https://i.imgur.com/YhGZ9Zp.png")
        .setTitle("**UŻYTKOWNICY ONLINE**")
        .setDescription(messageContent)
        .setColor("Blue")
        .setTimestamp()
        .setFooter({
          text: `!help`,
        });

      //Build Raport
      const raport = {
        embeds: [embed],
      };
      return raport;
    } catch (error) {
      console.error(error);
      return await messageBuilder.build(
        "error",
        "Wystąpił nieoczekiwany błąd.",
        ""
      );
    }
  },
};

function buildFor(groupColor, groupSign, groupName, playersData) {
  let onlineCount = 0;
  playersData.forEach((player) => {
    if (player.inGame) {
      onlineCount += 1;
    }
  });
  let fragment = `:${groupColor}_circle: **${groupName}** - ${onlineCount}\n`;

  if (playersData.length == 0) {
    fragment += `\n`;
    return fragment;
  }

  playersData.forEach((player) => {
    if (player.inGame) {
      fragment += `• (${groupSign}${player.admin.level}) **${player.name}**\n`;
    }
  });

  fragment += `\n`;
  return fragment;
}
