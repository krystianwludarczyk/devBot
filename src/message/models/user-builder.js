const api = require("../../api/api");
const { EmbedBuilder } = require("discord.js");
const messageBuilder = require("../message-builder");
const database = require("../../database/database");

module.exports = {
  build: async function (username) {
    try {
      //Variables
      const activity = await api.getUsersData();
      let messageContent = "";
      let targetUser;

      //Get User Data
      activity.forEach((player) => {
        if (player.name == username && player.admin.type == 2) {
          targetUser = player;
        }
      });
      if (targetUser == null)
        return await messageBuilder.build(
          "error",
          "Podany użytkownik nie istnieje!\nUpewnij się, że wprowadzona nazwa jest identyczna z tą na forum.",
          ""
        );

      //Status
      messageContent += `${
        targetUser.inGame == true ? ":green_circle:" : ":red_circle:"
      } **Status:** ${targetUser.inGame == true ? "Online" : "Offline"}\n\n`;
      messageContent += `:label: **Rola:** S${targetUser.admin.level}\n\n`;

      //Time
      messageContent += `:hourglass: **Czas:**\n`;
      messageContent += `• w grze: ${targetUser.gameTimeToday} minut\n`;
      messageContent += `• na służbie: ${targetUser.admin.duty} minut\n\n`;

      //Activity
      let percentageDuty;
      if (targetUser.admin.duty === 0 && targetUser.gameTimeToday === 0) {
        percentageDuty = 0;
      } else {
        percentageDuty = Math.min(
          100,
          Math.max(
            0,
            Math.round((targetUser.admin.duty / targetUser.gameTimeToday) * 100)
          )
        );
      }

      messageContent += `:bar_chart: **Aktywność:** ${percentageDuty}%\n\n`;
      const lastWeekActivityTime = await database.getUserActivity(
        targetUser.name,
        7
      );
      messageContent += `**:ledger: Ostatni tydzień:** ${lastWeekActivityTime} minut\n`;

      //Build Embed
      const embed = new EmbedBuilder()
        .setThumbnail("https://i.imgur.com/TYQ4iX7.png")
        .setURL(
          `https://devgaming.pl/profile/${targetUser.uid}-${targetUser.name}/`
        )
        .setTitle(`**${targetUser.name}** - .devGaming`)
        .setDescription(messageContent)
        .setColor("Blue")
        .setTimestamp()
        .setFooter({
          text: `!help`,
        });

      //Build Raport
      const raport = {
        content: "",
        embeds: [embed],
      };
      return raport;
    } catch (error) {
      console.error(error);
      return await messageBuilder.build(
        "error",
        "Wystąpił nieoczekiwany błąd.",
        ""
      );
    }
  },
};
