const helpBuilder = require("./models/help-builder");
const onlineBuilder = require("./models/online-builder");
const dailytopBuilder = require("./models/dailytop-builder");
const userBuilder = require("./models/user-builder");
const topBuilder = require("./models/top-builder");

module.exports = {
  buildReply: async function (message) {
    if (message.content == "!help") {
      return await helpBuilder.build();
    } else if (message.content == "!online") {
      return await onlineBuilder.build();
    } else if (message.content == "!dailytop") {
      return await dailytopBuilder.build();
    } else if (message.content.startsWith("!user")) {
      const args = message.content.split(" ");
      return await userBuilder.build(args[1]);
    } else if (message.content.startsWith("!top")) {
      const args = message.content.split(" ");
      return await topBuilder.build(args[1]);
    } else {
      return await helpBuilder.build();
    }
  },
};
