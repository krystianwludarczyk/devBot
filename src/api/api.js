const config = require("../../config.json");
const axios = require("axios");

module.exports = {
  getUsersData: getUsersData,
  getServerData: getServerData,
};

async function getServerData() {
  try {
    const response = await axios.get(config.apiUrlMTA);
    const servers = response.data;
    const serverData = await servers.find(
      (server) => server.ip === config.serverIP
    );
    return serverData;
  } catch (error) {
    console.log(error);
  }
}

async function getUsersData() {
  try {
    const loginResponse = await axios.post(
      config.apiUrlLogin,
      config.apiCredentials
    );
    const jwt = loginResponse.data.token;
    const activityResponse = await axios.get(config.apiUrlActivity, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
    const response = activityResponse.data;
    return response;
  } catch (error) {
    console.log(error);
  }
}
