const config = require("../config.json");
const { Client, IntentsBitField } = require("discord.js");
const replyBuilder = require("./message/reply-builder");
const cron = require("node-cron");
const api = require("./api/api");
const database = require("./database/database");
const dailytopBuilder = require("./message/models/dailytop-builder");
const messageBuilder = require("./message/message-builder");

async function run() {
  const discordClient = new Client({
    intents: [
      IntentsBitField.Flags.Guilds,
      IntentsBitField.Flags.GuildMessages,
      IntentsBitField.Flags.MessageContent,
    ],
  });

  discordClient.once("ready", () => {
    console.log("Bot started successfully!");
  });

  discordClient.on("messageCreate", async (message) => {
    if (message.content.startsWith("!")) {
      const reply = await replyBuilder.buildReply(message);
      message.reply(reply);
    }
  });

  await discordClient.login(config.botToken);

  cron.schedule("45 0 * * *", async () => {
    try {
      const channel = discordClient.channels.cache.get(config.userChannelID);
      channel.send(
        await messageBuilder.build(
          "info",
          "Za 15 minut podsumowanie aktywności. Zresetujcie swoje duty!",
          `<@&${config.userRoleID}>`
        )
      );
    } catch (error) {
      console.error(error);
    }
  });

  //Send Daily Raport
  cron.schedule("0 1 * * *", async () => {
    try {
      await database.pushUsersDailyActivity();
      const channel = discordClient.channels.cache.get(config.userChannelID);
      channel.send(await dailytopBuilder.build(`<@&${config.userRoleID}>`));
    } catch (error) {
      console.error(error);
    }
  });

  async function refreshPlayersCount() {
    try {
      const serverData = await api.getServerData();
      const currentPlayers = Number(serverData.players);

      if (!isNaN(currentPlayers)) {
        await database.pushCurrentPlayersCount(currentPlayers, new Date());
        return currentPlayers;
      } else {
        const lastPlayersCount = await database.getLastPlayersCount();
        return lastPlayersCount;
      }
    } catch (error) {
      console.log(error);
    }
  }

  //Update Bot Activity
  cron.schedule("* * * * *", async () => {
    try {
      const playersCount = await refreshPlayersCount();
      discordClient.user.setActivity({
        name: `${playersCount} graczy`,
      });
    } catch (error) {
      console.error(error);
    }
  });
}

run();
