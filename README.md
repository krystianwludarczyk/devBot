> **About project**

The project is a Discord bot created for the needs of the MTA:SA server. The bot responds to commands, creates high-quality Embed messages, and cyclically sends reports (about users activity) created based on data provided by the API.
The bot is capable of storing and manipulating data in a database using SQL queries.

> **Libraries used**

- npm
- node.js
- sqlite3
- axios
- node-cron
- discord.js v14

> **FIRST RUN**

1. DATABASE MIGRATION

```bash
node src/database/database-migration.js
```

2. CONFIGURATION

```bash
cp config-example.json config.json
or
copy config-example.json config.json
```

Now you need to edit the config.json file according to your individual requirements.

3. RUN PROJECT

```bash
node src/index.js
```
